#include "stdafx.h"
#include "idcrl.h"

#pragma comment(lib, "wininet.lib")
#pragma comment(lib, "crypt32.lib")

#define UTIL_SET_COPIED_STRING(dest, s) { \
	if (dest) { free((void*)dest); dest = NULL; } \
	if (s) { \
		size_t len = wcslen(s) + 1; \
		LPWSTR tmp = (LPWSTR)malloc(len * sizeof(WCHAR)); \
		wcscpy_s(tmp, len, s); \
		dest = tmp; \
	} \
}

#if 0
const LPCSTR ServerURL = "localhost";
const bool ServerUseHTTPS = false;
#else
const LPCSTR ServerURL = "m1.escargot.log1p.xyz";
const bool ServerUseHTTPS = true;
#endif
const LPCSTR ServerAction = "/NotRST.srf";

const LPCWSTR MyGUID = L"{3742D836-57A7-48C9-A38F-1DB43DDA0E5C}";
const DATA_BLOB MyKey = { 24, (BYTE*)"DnWUp6xdZDCDjgKXfXvRQ3jN" };

// https://msdn.microsoft.com/en-us/library/office/ff690568(v=office.14).aspx
// https://msdn.microsoft.com/en-us/library/office/hh472108(v=office.14).aspx
// https://github.com/davispuh/LiveIdentity

// MSN calls the enumeration functions to list persisted credentials:
// - first it calls to list accounts that have password saved (ps:password)
// - second it calls to list accounts that don't have password saved (ps:membernameonly)
// As the user selects a persisted credential from the list (or MSN does so automatically):
// - an identity handle is created for it (and saved for re-use)
// - HasPersistedCredential is called:
// 	- first to see if it has a password saved (ps:password)
// 	- secondly to check if that email is persisted (ps:membernameonly)
// When logging in, based on the remember me settings, PersistCredential is called:
// - if only "Remember Me", ps:membernameonly
// - if Remember Me+Password, ps:password
// - else, it's not called

LONG WINAPI MyRegGetValueW(
	_In_        HKEY    hkey,
	_In_opt_    LPCWSTR lpSubKey,
	_In_opt_    LPCWSTR lpValue,
	_In_opt_    DWORD   dwFlags,
	_Out_opt_   LPDWORD pdwType,
	_Out_opt_   PVOID   pvData,
	_Inout_opt_ LPDWORD pcbData
) {
	HKEY hKey2 = NULL;
	LONG s = RegOpenKeyExW(HKEY_CURRENT_USER, lpSubKey, 0, KEY_READ, &hKey2);
	if (s == ERROR_SUCCESS) {
		DWORD type = 0;
		s = RegQueryValueExW(hKey2, lpValue, NULL, &type, (BYTE*)pvData, pcbData);
		if (s == ERROR_SUCCESS) {
			if (dwFlags == RRF_RT_REG_BINARY && type != REG_BINARY) {
				s = -1;
			}
			if (dwFlags == RRF_RT_REG_SZ && type != REG_SZ) {
				s = -1;
			}
		}
	}
	if (hKey2) RegCloseKey(hKey2);
	return s;
}

HRESULT WINAPI CreateIdentityHandle(
	IN OPTIONAL LPCWSTR wszMemberName,
	IN DWORD dwflags,
	OUT PassportIdentityHandle *phIdentity
) {
	if (!phIdentity) return E_POINTER;

	PassportIdentityHandle h = (PassportIdentityHandle)malloc(sizeof(PIH));
	h->credMemberName = NULL;
	h->credMemberPass = NULL;
	h->success = FALSE;
	h->logonThread = NULL;
	h->cb = NULL;
	h->cbData = NULL;
	
	UTIL_SET_COPIED_STRING(h->credMemberName, wszMemberName);

	*phIdentity = h;
	return S_OK;
}

HRESULT WINAPI CloseIdentityHandle(IN PassportIdentityHandle hIdentity) {
	if (!hIdentity) return E_POINTER;
	free((void*)hIdentity->credMemberName);
	free((void*)hIdentity->credMemberPass);
	if (hIdentity->logonThread) {
		CloseHandle(hIdentity->logonThread);
	}
	hIdentity->credMemberName = NULL;
	hIdentity->credMemberPass = NULL;
	hIdentity->logonThread = NULL;
	hIdentity->cb = NULL;
	hIdentity->cbData = NULL;
	free(hIdentity);
	return S_OK;
}

HRESULT WINAPI CancelPendingRequest(IN PassportIdentityHandle hIdentity) {
	if (!hIdentity) return E_POINTER;
	hIdentity->success = FALSE;
	if (hIdentity->logonThread) CloseHandle(hIdentity->logonThread);
	hIdentity->logonThread = NULL;
	return S_OK;
}

HRESULT WINAPI SetCredential(IN PassportIdentityHandle hIdentity, IN LPCWSTR wszCredType, IN LPCWSTR wszCredValue) {
	if (!hIdentity) return E_POINTER;
	UTIL_SET_COPIED_STRING(hIdentity->credMemberPass, wszCredValue);
	return S_OK;
}

HRESULT WINAPI SetIdentityCallback(IN PassportIdentityHandle hIdentity, IN IdentityChangedCallback pfCallback, IN OPTIONAL void* pfCallbackData) {
	hIdentity->cb = pfCallback;
	hIdentity->cbData = pfCallbackData;
	return S_OK;
}

DWORD WINAPI LogonThread(LPVOID lpParam) {
	PassportIdentityHandle hIdentity = (PassportIdentityHandle)lpParam;

	hIdentity->success = FALSE;
	HINTERNET hi = InternetOpenA("Mozilla/5.0 (compatible; MSN-Escargot; +https://escargot.log1p.xyz)", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
	if (hi) {
		INTERNET_PORT port = (ServerUseHTTPS ? INTERNET_DEFAULT_HTTPS_PORT : INTERNET_DEFAULT_HTTP_PORT);
		HINTERNET hc = InternetConnectA(hi, ServerURL, port, NULL, NULL, INTERNET_SERVICE_HTTP, 0, 0);
		if (hc) {
			DWORD flags = (INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_NO_COOKIES | INTERNET_FLAG_NO_UI | INTERNET_FLAG_RELOAD | INTERNET_FLAG_PRAGMA_NOCACHE | INTERNET_FLAG_NO_AUTH);
			if (ServerUseHTTPS) flags |= INTERNET_FLAG_SECURE;
			HINTERNET hr = HttpOpenRequestA(hc, "POST", ServerAction, "HTTP/1.1", NULL, NULL, flags, NULL);
			if (hr) {
				WCHAR headers[1000];
				wsprintf(headers, L"X-User: %ls\r\nX-Password: %ls\r\n\r\n\r\n", hIdentity->credMemberName, hIdentity->credMemberPass);
				BOOL success = HttpSendRequestW(hr, headers, wcslen(headers), NULL, 0);
				if (success) {
					DWORD len = TOKEN_MAX_LEN * sizeof(WCHAR);
					wsprintf(hIdentity->token, L"X-Token");
					BOOL success = HttpQueryInfoW(hr, HTTP_QUERY_CUSTOM, hIdentity->token, &len, NULL);
					if (success) {
						hIdentity->success = TRUE;
					}
				}
			}
			InternetCloseHandle(hr);
		}
		InternetCloseHandle(hc);
	}
	InternetCloseHandle(hi);

	if (!hIdentity->logonThread) {
		// Request was cancelled
		return 0;
	}

	CloseHandle(hIdentity->logonThread);
	hIdentity->logonThread = NULL;

	IdentityChangedCallback cb = hIdentity->cb;
	if (cb) {
		cb(hIdentity, hIdentity->cbData, hIdentity->success);
	}

	return 0;
}

HRESULT WINAPI LogonIdentityEx(
	IN PassportIdentityHandle hIdentity, IN OPTIONAL LPCWSTR wszAuthPolicy,
	IN DWORD dwAuthFlags, IN PCRSTParams pcRSTParams, IN DWORD dwRSTParamsCount
) {
	if (!hIdentity) return E_POINTER;
	if (!hIdentity->credMemberName) return PPCRL_REQUEST_E_INVALID_MEMBER_NAME;
	if (!hIdentity->credMemberPass) return PPCRL_REQUEST_E_MISSING_PRIMARY_CREDENTIAL;

	CancelPendingRequest(hIdentity);
	hIdentity->logonThread = CreateThread(NULL, 0, LogonThread, hIdentity, 0, NULL);
	if (hIdentity->logonThread == NULL) {
		return E_ABORT;
	}
	return S_OK;
}

HRESULT WINAPI GetAuthStateEx(
	IN PassportIdentityHandle hIdentity,
	IN LPCWSTR wzServiceTarget,
	OUT OPTIONAL HRESULT * phrAuthState,
	OUT OPTIONAL HRESULT * phrAuthRequired,
	OUT OPTIONAL HRESULT * phrRequestStatus,
	OUT OPTIONAL LPWSTR * szWebFlowUrl
) {
	if (!hIdentity) return E_POINTER;

	bool success = hIdentity->success;

	if (phrAuthState) {
		*phrAuthState = (success ? 0x48803 : PPCRL_AUTHSTATE_E_UNAUTHENTICATED);
	}
	if (phrAuthRequired) {
		*phrAuthRequired = 0;
	}
	if (phrRequestStatus) {
		*phrRequestStatus = (success ? 0 : PPCRL_REQUEST_E_BAD_MEMBER_NAME_OR_PASSWORD);
	}
	if (szWebFlowUrl) {
		// This might be useful to tell users to make an account if they haven't already
		LPWSTR tmp = (LPWSTR)malloc(30 * sizeof(WCHAR));
		wcscpy_s(tmp, 30, L"");
		*szWebFlowUrl = tmp;
	}

	return S_OK;
}

HRESULT WINAPI GetExtendedError(
	IN PassportIdentityHandle hIdentity,
	IN LPVOID   pReserved,
	OUT DWORD   *pdwCategory,
	OUT DWORD   *pdwError,
	OUT OPTIONAL LPWSTR  *pszErrorBlob
) {
	if (!hIdentity) return E_POINTER;

	if (pdwCategory) {
		*pdwCategory = 0;
	}
	if (pdwError) {
		*pdwError = 0;
	}
	if (pszErrorBlob) {
		LPWSTR tmp = (LPWSTR)malloc(100 * sizeof(WCHAR));
		wcscpy_s(tmp, 100, L"pszErrorBlob-not-implemented");
		*pszErrorBlob = tmp;
	}

	return S_OK;
}

HRESULT WINAPI AuthIdentityToService(
	IN PassportIdentityHandle hIdentity,
	IN LPCWSTR szServiceTarget,
	IN OPTIONAL LPCWSTR szServicePolicy,
	IN DWORD dwTokenRequestFlags,
	OUT OPTIONAL LPWSTR *szToken,
	OUT OPTIONAL DWORD *pdwResultFlags,
	OUT OPTIONAL BYTE **ppbSessionKey,
	OUT OPTIONAL DWORD *pcbSessionKeyLength
) {
	if (!hIdentity) return E_POINTER;

	if (!hIdentity->success) {
		return PPCRL_E_UNABLE_TO_RETRIEVE_SERVICE_TOKEN;
	}

	if (szToken) {
		LPWSTR tmp = (LPWSTR)malloc(TOKEN_MAX_LEN * sizeof(WCHAR));
		wcscpy_s(tmp, TOKEN_MAX_LEN, hIdentity->token);
		*szToken = tmp;
	}
	// This seems to work
	if (pdwResultFlags) {
		// The real version seems to always return this
		*pdwResultFlags = 0x01;
	}
	if (ppbSessionKey) {
		*ppbSessionKey = 0;
	}
	if (pcbSessionKeyLength) {
		*pcbSessionKeyLength = 0;
	}

	return S_OK;
}

HRESULT WINAPI SetChangeNotificationCallback(LPCWSTR a0, DWORD a1, void* callback) {
	return S_OK;
}
HRESULT WINAPI RemoveChangeNotificationCallback() {
	return S_OK;
}

#pragma region Credential Persistence

const LPCWSTR CRED_TYPE_NAME = L"AppId:ps:membernameonly";
const LPCWSTR CRED_TYPE_PASSWORD = L"esc:password";
const LPCWSTR CRED_BASE = L"SOFTWARE\\Microsoft\\IdentityCRL\\Creds";

LPCWSTR TranslateCredType(IN OPTIONAL LPCWSTR wszCredType) {
	if (wszCredType == NULL) return NULL;
	if (lstrcmpW(L"ps:password", wszCredType) == 0) return CRED_TYPE_PASSWORD;
	return CRED_TYPE_NAME;
}

LPCWSTR CreateCredKey(IN PassportIdentityHandle hIdentity) {
	LPWSTR out = (LPWSTR)malloc(200 * sizeof(WCHAR));
	wsprintf(out, L"%ls\\%ls", CRED_BASE, hIdentity->credMemberName);
	return out;
}

HRESULT WINAPI HasPersistedCredential(IN PassportIdentityHandle hIdentity, IN LPCWSTR wszCredType, bool* out) {
	if (!hIdentity) return E_POINTER;
	*out = false;
	if (!hIdentity->credMemberName) return S_OK;
	wszCredType = TranslateCredType(wszCredType);
	LPCWSTR subkey = CreateCredKey(hIdentity);
	if (wszCredType == CRED_TYPE_PASSWORD) {
		DATA_BLOB blobIn;
		blobIn.pbData = NULL;
		blobIn.cbData = 0;
		LSTATUS s = MyRegGetValueW(HKEY_CURRENT_USER, subkey, wszCredType, RRF_RT_REG_BINARY, NULL, NULL, &blobIn.cbData);
		if (s == ERROR_SUCCESS) {
			blobIn.pbData = (BYTE*)malloc(blobIn.cbData);
			MyRegGetValueW(HKEY_CURRENT_USER, subkey, wszCredType, RRF_RT_REG_BINARY, NULL, blobIn.pbData, &blobIn.cbData);
			if (s == ERROR_SUCCESS) {
				DATA_BLOB blobOut;
				blobOut.pbData = NULL;
				blobOut.cbData = 0;
				BOOL success = CryptUnprotectData(&blobIn, NULL, (DATA_BLOB*)&MyKey, NULL, NULL, CRYPTPROTECT_UI_FORBIDDEN, &blobOut);
				if (success) {
					if (!hIdentity->credMemberPass) {
						// If password is already set, don't set it again. If we were to set it again,
						// this causes a bug: when MSN tries logging in with a saved password that is *wrong*,
						// and user types in the *new* password, MSN will call SetCredential with the *new*
						// password, but call HasPersistedCredential immediately after.
						BYTE* tmp = (BYTE*)malloc(blobOut.cbData);
						memcpy(tmp, blobOut.pbData, blobOut.cbData);
						hIdentity->credMemberPass = (LPCWSTR)tmp;
					}
					s = ERROR_SUCCESS;
				}
				if (blobOut.pbData) LocalFree(blobOut.pbData);
			}
			free((void*)blobIn.pbData);
		}
		*out = (s == ERROR_SUCCESS);
	} else {
		LSTATUS s = MyRegGetValueW(HKEY_CURRENT_USER, subkey, wszCredType, RRF_RT_REG_SZ, NULL, NULL, NULL);
		*out = (s == ERROR_SUCCESS);
	}
	free((void*)subkey);
	return S_OK;
}

HRESULT WINAPI PersistCredential(IN PassportIdentityHandle hIdentity, IN LPCWSTR wszCredType) {
	if (!hIdentity) return E_POINTER;
	if (!hIdentity->credMemberName) return S_OK;
	wszCredType = TranslateCredType(wszCredType);
	LPCWSTR subkey = CreateCredKey(hIdentity);

	HKEY hKey = NULL;
	LONG s = RegCreateKeyEx(HKEY_CURRENT_USER, subkey, 0, NULL, 0, KEY_WRITE, NULL, &hKey, NULL);
	if (s == ERROR_SUCCESS) {
		if (wszCredType == CRED_TYPE_PASSWORD) {
			LPCWSTR pw = hIdentity->credMemberPass;
			if (pw) {
				DATA_BLOB blobIn;
				blobIn.pbData = (BYTE*)pw;
				blobIn.cbData = (wcslen(pw) + 1) * sizeof(WCHAR);

				DATA_BLOB blobOut;
				blobOut.pbData = NULL;
				blobOut.cbData = 0;
				BOOL success = CryptProtectData(&blobIn, NULL, (DATA_BLOB*)&MyKey, NULL, NULL, CRYPTPROTECT_UI_FORBIDDEN, &blobOut);
				if (success) {
					RegSetValueExW(hKey, wszCredType, 0, REG_BINARY, blobOut.pbData, blobOut.cbData);
				}
				if (blobOut.pbData) LocalFree(blobOut.pbData);
			}
		} else {
			LPCWSTR value = MyGUID;
			RegSetValueExW(hKey, wszCredType, 0, REG_SZ, (BYTE*)value, 2 * (wcslen(value) + 1));
		}
	}
	if (hKey) RegCloseKey(hKey);
	free((void*)subkey);

	return S_OK;
}

HRESULT WINAPI RemovePersistedCredential(IN PassportIdentityHandle hIdentity, IN LPCWSTR wszCredType) {
	if (!hIdentity) return E_POINTER;
	if (!hIdentity->credMemberName) return S_OK;
	
	// When MSN says "remove ps:password credential", it really means remove password AND email.
	//wszCredType = TranslateCredType(wszCredType);
	wszCredType = CRED_TYPE_NAME;

	LPCWSTR subkey = CreateCredKey(hIdentity);
	if (wszCredType == CRED_TYPE_PASSWORD) {
		// Delete only the value under subkey
		HKEY hKey = NULL;
		LONG s = RegCreateKeyEx(HKEY_CURRENT_USER, subkey, 0, NULL, 0, KEY_SET_VALUE, NULL, &hKey, NULL);
		if (s == ERROR_SUCCESS) {
			RegDeleteValueW(hKey, wszCredType);
		}
		if (hKey) RegCloseKey(hKey);
	} else {
		// Delete entire subkey
		// Should Use RegDeleteTreeW, but that's not available on XP.
		RegDeleteKeyW(HKEY_CURRENT_USER, subkey);
	}
	free((void*)subkey);
	return S_OK;
}

HRESULT WINAPI EnumIdentitiesWithCachedCredentials(
	IN OPTIONAL LPCWSTR szCachedCredType,
	OUT PassportEnumIdentitiesHandle *peihEnumHandle
) {
	if (peihEnumHandle == NULL) return E_INVALIDARG;
	PassportEnumIdentitiesHandle h = (PassportEnumIdentitiesHandle)malloc(sizeof(PEIH));
	h->root = NULL;
	h->current = NULL;
	*peihEnumHandle = h;

	HKEY hKey = NULL;
	LONG s = RegCreateKeyEx(HKEY_CURRENT_USER, CRED_BASE, 0, NULL, 0, KEY_ENUMERATE_SUB_KEYS, NULL, &hKey, NULL);
	if (s != ERROR_SUCCESS) {
		if (hKey) RegCloseKey(hKey);
		return S_OK;
	}

	szCachedCredType = TranslateCredType(szCachedCredType);
	WCHAR buf[MEMBER_NAME_MAX_LENGTH];
	for (DWORD i = 0; i < 30; ++i) {
		DWORD bufsize = MEMBER_NAME_MAX_LENGTH;
		LONG s = RegEnumKeyEx(hKey, i, buf, &bufsize, NULL, NULL, NULL, NULL);
		if (s == ERROR_NO_MORE_ITEMS) break;
		if (s == ERROR_SUCCESS) {
			LSTATUS s = MyRegGetValueW(hKey, buf, CRED_TYPE_PASSWORD, RRF_RT_REG_BINARY, NULL, NULL, NULL);
			if ((s == ERROR_SUCCESS) != (szCachedCredType == CRED_TYPE_PASSWORD)) continue;
			IdentityEntry* ie = (IdentityEntry*)malloc(sizeof(IdentityEntry));
			wcscpy_s(ie->szMemberName, buf);
			ie->next = h->root;
			h->root = ie;
		}
	}
	h->current = h->root;

	RegCloseKey(hKey);

	return S_OK;
}

HRESULT WINAPI CloseEnumIdentitiesHandle(
	IN PassportEnumIdentitiesHandle hEnumHandle
) {
	if (!hEnumHandle) return S_OK;
	IdentityEntry* current = hEnumHandle->root;
	hEnumHandle->root = NULL;
	hEnumHandle->current = NULL;
	free(hEnumHandle);
	while (current) {
		IdentityEntry* next = current->next;
		free(current);
		current = next;
	}
	return S_OK;
}

HRESULT WINAPI NextIdentity(
	IN PassportEnumIdentitiesHandle hEnumHandle,
	OUT LPWSTR * szMemberName
) {
	if (szMemberName == NULL) return E_POINTER;
	*szMemberName = NULL;
	IdentityEntry* current = hEnumHandle->current;
	if (current == NULL || !current->szMemberName) {
		return PPCRL_S_NO_MORE_IDENTITIES;
	}
	hEnumHandle->current = current->next;
	size_t len = wcslen(current->szMemberName) + 1;
	LPWSTR tmp = (LPWSTR)malloc(len * sizeof(WCHAR));
	wcscpy_s(tmp, len, current->szMemberName);
	*szMemberName = tmp;
	return S_OK;
}

#pragma endregion

HRESULT WINAPI PassportFreeMemory(IN void * pMemoryToFree) {
	free(pMemoryToFree);
	return S_OK;
}

HRESULT WINAPI Initialize(GUID* rguid, int x, int y) {
	return S_OK;
}

HRESULT WINAPI InitializeEx(
	IN REFGUID GuidClientApplication,
	IN LONG lPPCRLVersion,
	IN DWORD dwFlags,
	IN LPIDCRL_OPTION pOptions,
	IN DWORD dwOptions
) {
	return S_OK;
}

HRESULT WINAPI Uninitialize() {
	return S_OK;
}

HRESULT WINAPI EncryptWithSessionKey(
	IN PassportIdentityHandle hIdentity,
	IN LPCWSTR wszServiceName,
	IN DWORD dwAlgIdEncrypt,
	IN DWORD dwAlgIdHash,
	IN LPVOID pbData, IN DWORD dwDataSize,
	OUT PBYTE* pbCipher, OUT PDWORD pdwCipherSize
) {
	// Apparently pbCipher is a byte**?
	*pbCipher = NULL;
	*pdwCipherSize = 0;
	return S_OK;
}

HRESULT WINAPI GetIdentityPropertyByName(
	IN PassportIdentityHandle hIdentity,
	IN LPWSTR wszPropertyName,
	OUT LPWSTR* pwszPropertyValue
) {
	LPWSTR tmp = (LPWSTR)malloc(sizeof(WCHAR));
	tmp[0] = L'\0';
	*pwszPropertyValue = tmp;
	return S_OK;
}

#undef UTIL_SET_COPIED_STRING

#pragma region Dummy functions to fill up ordinals
HRESULT WINAPI AuthIdentityToServiceEx(PassportIdentityHandle, DWORD, PCRSTParams, DWORD) { return S_OK; }
HRESULT WINAPI BuildAuthTokenRequest() { return S_OK; }
HRESULT WINAPI BuildAuthTokenRequestEx() { return S_OK; }
HRESULT WINAPI BuildServiceTokenRequest() { return S_OK; }
HRESULT WINAPI BuildServiceTokenRequestEx() { return S_OK; }
HRESULT WINAPI CreatePassportAuthUIContext() { return S_OK; }
HRESULT WINAPI DestroyPassportAuthUIContext() { return S_OK; }
HRESULT WINAPI GetAuthState(PassportIdentityHandle, HRESULT*, HRESULT*, HRESULT*, LPWSTR) { return S_OK; }
// Can't export GetCertificate, because that confuses VS and causes it
// to import GDI32.dll's GetCertificate (but doesn't actually use it).
HRESULT WINAPI x_GetCertificate() { return S_OK; }
HRESULT WINAPI GetIdentityProperty() { return S_OK; }
HRESULT WINAPI GetPreferredAuthUIContextSize() { return S_OK; }
HRESULT WINAPI GetWebAuthUrl(PassportIdentityHandle, LPCWSTR, LPCWSTR, LPCWSTR, LPCWSTR, LPWSTR*, LPWSTR*) { return S_OK; }
HRESULT WINAPI LogonIdentity() { return S_OK; }
HRESULT WINAPI LogonIdentityWithUI() { return S_OK; }
HRESULT WINAPI MoveAuthUIContext() { return S_OK; }
HRESULT WINAPI PutTokenResponse() { return S_OK; }
HRESULT WINAPI PutTokenResponseEx() { return S_OK; }
HRESULT WINAPI SetIdentityProperty() { return S_OK; }
HRESULT WINAPI VerifyCertificate() { return S_OK; }
HRESULT WINAPI CacheAuthState() { return S_OK; }
HRESULT WINAPI CloseDeviceID() { return S_OK; }
HRESULT WINAPI CreateIdentityHandleFromAuthState() { return S_OK; }
HRESULT WINAPI CreateIdentityHandleFromCachedAuthState() { return S_OK; }
HRESULT WINAPI CreateLinkedIdentityHandle() { return S_OK; }
HRESULT WINAPI DecryptWithSessionKey() { return S_OK; }
HRESULT WINAPI DeleteTweenerCreds() { return S_OK; }
HRESULT WINAPI EncryptTweenerPasswordForCredman() { return S_OK; }
HRESULT WINAPI EnumerateCertificates() { return S_OK; }
HRESULT WINAPI EnumerateDeviceID() { return S_OK; }
HRESULT WINAPI ExportAuthState() { return S_OK; }
HRESULT WINAPI GenerateCertToken() { return S_OK; }
HRESULT WINAPI GenerateDeviceToken() { return S_OK; }
HRESULT WINAPI GetAssertion() { return S_OK; }
HRESULT WINAPI GetDeviceId() { return S_OK; }
HRESULT WINAPI GetServiceConfig(LPCWSTR, LPWSTR*) { return S_OK; }
HRESULT WINAPI GetWebAuthUrlEx() { return S_OK; }
HRESULT WINAPI InitializeApp() { return S_OK; }
HRESULT WINAPI IsDeviceIDAdmin() { return S_OK; }
HRESULT WINAPI MigratePersistedCredentials() { return S_OK; }
HRESULT WINAPI OpenAuthenticatedBrowser() { return S_OK; }
HRESULT WINAPI RemoveAuthStateFromCache() { return S_OK; }
HRESULT WINAPI SaveTweenerCreds() { return S_OK; }
HRESULT WINAPI SetDeviceConsent() { return S_OK; }
HRESULT WINAPI GetExtendedProperty(LPCWSTR, LPCWSTR) { return S_OK; }
HRESULT WINAPI SetExtendedProperty(LPCWSTR, LPCWSTR) { return S_OK; }
HRESULT WINAPI SetIdcrlOptions() { return S_OK; }
HRESULT WINAPI GetUserExtendedProperty(LPCWSTR, LPCWSTR, LPCWSTR) { return S_OK; }
HRESULT WINAPI SetUserExtendedProperty(LPCWSTR, LPCWSTR, LPCWSTR) { return S_OK; }
HRESULT WINAPI VerifyAssertion() { return S_OK; }
void WINAPI Dummy1() {}
void WINAPI Dummy2() {}
#pragma endregion
